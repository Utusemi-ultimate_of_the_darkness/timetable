# Timetable

This is the Timetable for the 4th semester of the Mechanical CAD Design Course at Tohoku Electronics College.

## How to use this Timetable

1. Download your favorite version of the zip file [hear](https://github.com/UtusemiUltimate-of-the-darkness/timetable/releases)  
Or run the following in a terminal:  
    `$ git clone https://github.com/UtusemiUltimate-of-the-darkness/Timetable.git`  
2. If downloaded zip file, unzip downloaded zip file on your any place.
3. Open the html files in the unzipped folder on your browser.

### How to use command mode

1. Right click on the page.  
You'll see a prompt like the one below.  
<img src="img/html/displayPrompt.png" width="500px">

2. Enter a command at the prompt. The list of commands is shown in the following table. command is case-insensitive.  
    <table>
      <tr>
        <th>command</th>
        <th>description</th>
      </tr>
      <tr>
        <td>
          <div>''</div>
          <div>'menu'</div>
          <div>'context'</div>
          <div>'contextmenu'</div>
        </td>
        <td>Show context menu.</td>
      </tr>
      <tr>
        <td>
          <div>'display:block'</div>
          <div>'style.display:block'</div>
          <div>'body.display:block'</div>
          <div>'body.style.display:block'</div>
          <div>'timetable.display:block'</div>
          <div>'timetable.style.display:block'</div>
          <div>'.timetable.display:block'</div>
          <div>'.timetable.style.display:block'</div>
          <div>'body.timetable.style.display:block'</div>
        </td>
        <td>Show timetable.</td>
      </tr>
      <tr>
        <td>
          <div>'display:none'</div>
          <div>'style.display:none'</div>
          <div>'body.display:none'</div>
          <div>'body.style.display:none'</div>
          <div>'timetable.display:none'</div>
          <div>'timetable.style.display:none'</div>
          <div>'.timetable.display:none'</div>
          <div>'.timetable.style.display:none'</div>
          <div>'body.timetable.display:none'</div>
          <div>'body.timetable.style.display:none'</div>
        </td>
        <td>
          Hide timetable<br>
          (background appreciation mode).
        </td>
      </tr>
      <tr>
        <td>
          <div>'main.display:block'</div>
          <div>'main.style.display:block'</div>
          <div>'.main.display:block'</div>
          <div>'.main.style.display:block'</div>
          <div>'body.main.display:block'</div>
          <div>'body.main.style.display:block'</div>
          <div>'timetable.main.display:block'</div>
          <div>'timetable.main.style.display:block'</div>
          <div>'.timetable.main.display:block'</div>
          <div>'.timetable.main.style.display:block'</div>
          <div>'body.timetable.main.display:block'</div>
          <div>'body.timetable.main.style.display:block'</div>
        </td>
        <td>Show main timetable.</td>
      </tr>
      <tr>
        <td>
          <div>'main.display:none'</div>
          <div>'main.style.display:none'</div>
          <div>'.main.display:none'</div>
          <div>'.main.style.display:none'</div>
          <div>'timetable.main.display:none'</div>
          <div>'timetable.main.style.display:none'</div>
          <div>'.timetable.main.display:none'</div>
          <div>'.timetable.main.style.display:none'</div>
          <div>'body.timetable.main.display:none'</div>
          <div>'body.timetable.main.style.display:none'</div>
        </td>
        <td>Hide main timetable.</td>
      </tr>
      <tr>
        <td>
          <div>'print'</div>
          <div>'display:print'</div>
          <div>'style.display:print'</div>
          <div>'body.display:print'</div>
          <div>'body.style.display:print'</div>
          <div>'timetable.display:print'</div>
          <div>'timetable.style.display:print'</div>
          <div>'.timetable.display:print'</div>
          <div>'.timetable.style.display:print'</div>
          <div>'body.timetable.display:print'</div>
          <div>'body.timetable.style.display:print'</div>
        </td>
        <td>Show print mode</td>
      </tr>
      <tr>
        <td>
          <div>'disp'</div>
          <div>'display'</div>
          <div>'display:disp'</div>
          <div>'display:display'</div>
          <div>'style.display:display'</div>
          <div>'body.display:display'</div>
          <div>'body.style.display:display'</div>
          <div>'timetable.display:display'</div>
          <div>'timetable.style.display:display'</div>
          <div>'.timetable.display:display'</div>
          <div>'.timetable.style.display:display'</div>
          <div>'body.timetable.display:display'</div>
          <div>'body.timetable.style.display:display'</div>
        </td>
        <td>Show display mode</td>
      </tr>
    </table>


### version info

#### [4.3.1](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/4.3.1)

- Add a clock to display the current time.

#### [4.3.0](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/4.3.0)

- Update timetable (Survey of CG moves from mon1 to tue4, switch from online to regular lessons)

#### [4.2.5](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/4.2.5)

- Update Room 1107 seating chart (wed3)
- Adjusted size of each cell

#### [4.2.4](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/4.2.4)

- Fixed broken link for YearlyEventSchedule.pdf

#### [4.2.3](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/4.2.3)

- Delete the text-tools for Presentation on wed1 and wed5.
- Fixed the text-tools for CG practice on thu4.
- Fixed the background image for CG practice cell on thu4.

#### [4.2.2](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/4.2.2)

- Fixed the seating chart for Mechanical engineering II on wed3.

#### [4.2.1](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/4.2.1)

- Fixed seatingChart on wed3.

#### [4.2.0](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/4.2.0)

- Added seating chart for room 1107 used for mechanical engineering II.

#### [4.1.0](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/4.1.0)

- Add online link
- Add notebook link

#### [4.0.1](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/4.0.1)

- Add text-img to background of mon4 cell.

#### [4.0.0](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/4.0.0)

- Timetable for 4semester.

#### [3.8.7](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.8.7)

- Added online timetable.

#### [3.8.6](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.8.6)

- Improved only the table can scroll horizontally when page overflow to horizontally.
- Fixed the cell contents of thu5.

#### [3.8.5](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.8.5)

- Added a link to YearlyEventSchedule.pdf
-
#### [3.8.4](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.8.4)

- Added function that seating chart for classroom 1107 (wed3).

#### [3.8.3](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.8.3)

- Correction of classroom number display.
- Create a seating chart for classroom 1107 (wed3).
- Update online classroom link that thu3, thu4 and thu5 cell.

#### [3.8.2](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.8.2)

- Add pdf link to mon4 cell.
- Add note link to wed3 cell.
- Add online classroom link to thu3, thu4 and thu5 cell.
- Fixed a bug that header date is displayed when printing.

#### [3.8.1](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.8.1)

- Fixed description on README.md.
- Fixed a bug that the date in the header is not displayed when switching from print mode to display mode.

#### [3.8.0](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.8.0)

- Fixed class room of wed3.

#### [3.7.2](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.7.2)

- Adjust the appearance.

#### [3.7.1](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.7.1)

- Adjusting the appearance.

#### [3.7.0](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.7.0)

- Add text-image to subject cell background.
- Display text-tool of subject.
- Fixed class room of Survey of car.

#### [3.6.4](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.6.4)

- Add text-image to subject cell background.
- Display text-tool of subject.

#### [3.6.3](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.6.3)

- Display update date and time.

#### [3.6.2](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.6.2)

- Hidden online timetable.

#### [3.6.1](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.6.1)

- Emphasize today's day of the week line.

#### [3.6.0](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.6.0)

- Fixed subject of online timetable.

#### [3.5.0](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.5.0)

- Fixed subject of main timetable.
- Add distribute pdf file of timetable from school.

#### [3.4.0](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.4.0)

- Add online timetable (0430).

#### [3.3.3](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.3.3)

- Add courses to main timetable.

#### [3.2.1](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.2.1)

- Changed subject name.

#### [3.2.0](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.2.0)

- Add courses to online timetable.

#### [3.1.0](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.1.0)

- Updated online timetable.
- Improved to automatic display switching of online timetable.
- Implementation of display mode and print mode.
- Added the command.

#### [3.0.3](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.0.3)

- Changed page-title and body-title to auto-sync.

#### [3.0.2](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.0.2)

- Added command variation.
- Changed description of 'How to use this Timetable' in README.md

#### [3.0.1](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.0.1)

- Add timetable of online version.

#### [3.0.0](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/3.0.0)

- timetable 3semester ( template )

#### [2.5.2](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.5.2)

- Change text-tool of conputer basics(1-3).

#### [2.5.1](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.5.1)

- Fixed a bug in which the background color was printed when printing.

#### [2.5.0](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.5.0)

- Delete subject CG application first. (4-6, 5-6)
- Delete 6th period.

#### [2.4.3](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.4.3)

- Change textbook of cell conputerBasics.

#### [2.4.2](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.4.2)

- Display the background of each cell when printing.
- Adjusting the height of each cell.

#### [2.4.1](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.4.1)

- Bug fixed (display text of wed4, thu2 and thu3).

#### [2.4.0](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.4.0)

- Add text to CADpractice(Ms. N.Ito).

#### [2.3.3](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.3.3)

- View limited users on smartphone and tablet.

#### [2.3.2](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.3.2)

- Print style settings.

#### [2.3.1](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.3.1)

- Changed room name to bold.
- Display teacher's name.
- Display textbooks and tools.

#### [2.3.0](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.3.0)

- Removed online timetable.
- Fixed the list of commands in README.

#### [2.2.5](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.2.5)

- Removed class start date for CG appication first.
- Fixed the list of commands in README.

#### [2.2.4](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.2.4)

- Extended command mode commands.

#### [2.2.3](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.2.3)

- Fixed the problem of switching the timetable display.
- Add display contextMenu command.

#### [2.2.2](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.2.2)

- delete room description on online timetable.
- Add teachers name on online timetable.

#### [2.2.1](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.2.1)

- Supports command operations
- Change term of th online timetable
- Change subject name ( toLowerCase ).

#### [2.2.0](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.2.0)

- Add online timetable ( 1027-1102 )
- Change table position ( to center )

#### [2.1.1](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.1.1)

- Add textbook image to timetable cell background

#### [2.1.0](https://github.com/UtusemiUltimate-of-the-darkness/timetable/tree/2.1.0)

- Add background image
- language to English
- change text font

#### 2.0.0

- timetable of 2semester
